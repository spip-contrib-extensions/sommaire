# Changelog

## 2.0.0 - 2025-02-27

### Changed

- Compatible SPIP 4.2 minimum
- Chaînes de langue au format SPIP 4.1+

## 1.5.0 - 2024-09-17

### Added

- #24 Raccourci `<retire_sommaire|>` fonctionne en plus de `<retire_sommaire>`
- #23 Compatibilité avec le plugin `inserer_modeles`
- #12 Constante `SOMMAIRE_AUTO_OFF` pour désactiver l'insertion automatique au niveau du squelette

## 1.4.1 - 2024-07-18

### Fixed

- #21 SVG dans les titres potentiellement pétés

## 1.4.0 - 2024-07-05

### Added

- Compatibilité SPIP 4.3 et >

## 1.3.5 - 2023-05-04

### Added
- Compatibilité SPIP 4.2
- #4 `<retire_sommaire>` permet de ne pas insérer un sommaire dans un article spécifique
