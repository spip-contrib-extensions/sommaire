<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sommaire?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// S
	'sommaire_description' => 'إنشاء فهرس آلي للمقالاتز',
	'sommaire_nom' => 'فهرس آلي',
	'sommaire_slogan' => 'فهرس لمقالاتكم',
];
