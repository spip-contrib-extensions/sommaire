<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sommaire?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// S
	'sommaire_description' => 'Genera un índice automático de los artículos.',
	'sommaire_nom' => 'Índice automático',
	'sommaire_slogan' => 'Un índice para tus artículos',
];
