<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/sommaire.git

return [

	// S
	'sommaire_description' => 'Générer un sommaire automatique pour les articles.',
	'sommaire_nom' => 'Sommaire automatique',
	'sommaire_slogan' => 'Un sommaire pour vos articles',
];
