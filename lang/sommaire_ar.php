<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sommaire?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_exemple' => 'مثال',
	'cfg_exemple_explication' => 'تفسير المثال',
	'cfg_titre_parametrages' => 'إعدادات',

	// E
	'explication_niveau_max' => 'الحد الأقصى لعمق الفهرس. يمكن تغيير هذه القيمة حسب الطلب باستخدام علامة <code>#SOMMAIRE{#TEXTE,2}</code> في الصفحات النموذجية او <code>&lt;sommaire|niveau_max=2&gt;</code> في نص المقالات.',
	'explication_numerotation_sommaire' => 'يظهر الفهرس كقائمة نقطية او قائمة مرقمة.',
	'explication_sommaire_automatique_off' => 'الفهرس مدرج في الصفحة النموذجية من قبل المشرف على الموقع.', # MODIF
	'explication_sommaire_automatique_ondemand' => 'لا يتم إدراج الفهرس الا بوجود المختصر <tt>&lt;sommaire&gt;</tt> في نص المقالات.',

	// L
	'label_sommaire_automatique' => 'إدراج الفهرس في المقال',
	'label_sommaire_automatique_numerote' => 'نوع قائمة الفهرس',
	'label_sommaire_automatique_numerote_off' => 'قائمة نقطية',
	'label_sommaire_automatique_numerote_on' => 'قائمة مرقمة',
	'label_sommaire_automatique_off' => 'عدم إدراج في نص المقالات',
	'label_sommaire_automatique_on' => 'إدراج آلي في كل المقالات',
	'label_sommaire_automatique_ondemand' => 'إدراج في نص المقالات حسب الطلب',
	'label_sommaire_niveau_max' => 'العمق',
	'label_sommaire_niveau_max_1' => 'مستوى واحد',
	'label_sommaire_niveau_max_2' => 'مستويان',
	'label_sommaire_niveau_max_3' => '٣ مستويات',
	'label_sommaire_niveau_max_4' => '٤ مستويات',
	'label_sommaire_niveau_max_5' => '٥ مستويات',
	'label_sommaire_niveau_max_6' => '٦ مستويات',

	// S
	'sommaire_titre' => 'فهرس آلي',

	// T
	'titre_cadre_sommaire' => 'الفهرس',
	'titre_page_configurer_sommaire' => 'فهرس آلي',
	'titre_retour_sommaire' => 'عودة الى الفهرس',
];
