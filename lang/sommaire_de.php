<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sommaire?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_exemple' => 'Beispiel',
	'cfg_exemple_explication' => 'Erläuterung dieses Beipiels',
	'cfg_titre_parametrages' => 'Einstellungen',

	// E
	'explication_niveau_max' => 'Maximale Verschachtelungstiefe der Inhaltsverzeichnisse. Dieser Wert kann sich je nach Verwendung des Tags <code>#SOMMAIRE{#TEXTE,2}</code> in den Skeletten, oder entsprechend  <code>&lt;sommaire|niveau_max=2&gt;</code> im Artikeltext ändern.',
	'explication_numerotation_sommaire' => 'Das Inhaltsverzeichnis kann als nummerierte Liste oder als Punkt-Liste formatiert werden.',
	'explication_sommaire_automatique_off' => 'Das Inhaltsverzeichnis wird vom Webmaster in das Skelett eingebunden.',
	'explication_sommaire_automatique_ondemand' => 'Das Inhaltsverzeichnis wird nur bei Vorhandensen des Tags<tt>&lt;sommaire&gt;</tt> in den Text von Artikeln eingefügt.',

	// L
	'label_sommaire_automatique' => 'Einfügen des Artikel-Inhaltsverzeichnis',
	'label_sommaire_automatique_numerote' => 'Art der Liste des Inhaltsverzeichnis',
	'label_sommaire_automatique_numerote_off' => 'Punkt-Liste',
	'label_sommaire_automatique_numerote_on' => 'nummerierte Liste',
	'label_sommaire_automatique_off' => 'Kein Einfügen in Artikel',
	'label_sommaire_automatique_on' => 'Automatisches Einfügen in alle Artikel',
	'label_sommaire_automatique_ondemand' => 'Einfügen in Texte nach Bedarf',
	'label_sommaire_niveau_max' => 'Tiefe',
	'label_sommaire_niveau_max_1' => '1 Ebene',
	'label_sommaire_niveau_max_2' => '2 Ebenen',
	'label_sommaire_niveau_max_3' => '3 Ebenen',
	'label_sommaire_niveau_max_4' => '4 Ebenen',
	'label_sommaire_niveau_max_5' => '5 Ebenen',
	'label_sommaire_niveau_max_6' => '6 Ebenen',
	'label_sommaire_retour' => 'Ziel der Zurück-Links',
	'label_sommaire_retour_haut' => 'Über dem Inhaltsverzeichnis',
	'label_sommaire_retour_titre' => 'Über dem Titel im Inhaltsverzeichnis',

	// S
	'sommaire_titre' => 'Automatisches Inhaltsverzeichnis',

	// T
	'titre_cadre_sommaire' => 'Inhaltsverzeichnis',
	'titre_page_configurer_sommaire' => 'Automatisches Inhaltsverzeichnis',
	'titre_retour_sommaire' => 'Zurück zum Inhaltsverzeichnis',
];
