<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/sommaire?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_exemple' => 'Example',
	'cfg_exemple_explication' => 'Explanation of this example',
	'cfg_titre_parametrages' => 'Configuration',

	// E
	'explication_niveau_max' => 'Maximum depth of tables of contents. This value can be set on a case by case basis with the use of the tag <code>#SOMMAIRE{#TEXTE,2}</code> in SPIP templates, or with <code>&lt;sommaire|niveau_max=2&gt;</code> in the body of articles.',
	'explication_numerotation_sommaire' => 'The table of contents can be presented as a bulleted list of a numbered list.',
	'explication_sommaire_automatique_off' => 'The table of contents is embedded in the HTML template by the webmaster.', # MODIF
	'explication_sommaire_automatique_ondemand' => 'The table of contents is embedded only with the short code <tt>&lt;sommaire&gt;</tt> in the body of articles.',

	// L
	'label_sommaire_automatique' => 'Embed the article’s table of contents',
	'label_sommaire_automatique_numerote' => 'Type of list for the table of contents',
	'label_sommaire_automatique_numerote_off' => 'Bulleted list',
	'label_sommaire_automatique_numerote_on' => 'Numbered list',
	'label_sommaire_automatique_off' => 'No embed in articles body',
	'label_sommaire_automatique_on' => 'Automatic embed in all articles',
	'label_sommaire_automatique_ondemand' => 'Embed on demand in articles',
	'label_sommaire_niveau_max' => 'Depth',
	'label_sommaire_niveau_max_1' => '1 level',
	'label_sommaire_niveau_max_2' => '2 levels',
	'label_sommaire_niveau_max_3' => '3 levels',
	'label_sommaire_niveau_max_4' => '4 levels',
	'label_sommaire_niveau_max_5' => '5 levels',
	'label_sommaire_niveau_max_6' => '6 levels',

	// S
	'sommaire_titre' => 'Automatic table of contents',

	// T
	'titre_cadre_sommaire' => 'Table of contents',
	'titre_page_configurer_sommaire' => 'Automatic table of contents',
	'titre_retour_sommaire' => 'Back to the table of contents',
];
